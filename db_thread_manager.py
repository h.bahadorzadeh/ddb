from config import config
from queue import q
from log import log
from db_thread import dbThread
from time import time
import importlib

db_log = log(config)

importlib.import_module(config.get("db").get("db_object"))
eval("db_object = "+config.get("db").get("db_object"))

class dbThreadManager ():
    def __init__(self, config):
        self.logger = log.addLogger("dbManager")
        self.db_parameters = {
                            'host':     config.get('db').get('host'),
                            'user':     config.get('db').get('user'),
                            'passwd':   config.get('db').get('passwd'),
                            'port':     int(config.get('db').get('port')),
                            'db':       config.get('db').get('db')
                            }
        self.dbConnectionLimit = int(config.get('db').get('connectionLimit'))
        self.db_object = db_object
        self.start()
        # safeMode = False
        # try:
        #     safeMode = config.getint('db', 'safeMode')
        # except Exception as e:
        #     pass
    def start(self):
        safeMode = False
        self.db = []
        self.q = q()
        self.result = {}
        while len(self.db) < self.dbConnectionLimit:
            try:
                self.db.append(dbThread(self, safeMode))
                num = len(self.db)-1
                self.db[num].start()
                self.db[num]._id=num
            except Exception as e:
                self.logger.exception("getting conncetion")
            self.logger.info("db thread created")

#     def getConfig(self):
#         pass

    def __getattr__(self, name):
        self.logger.info("%s" % name)
        try:    
            def handlerFunction(*args, **kwargs):
                try:
                    tmp = {'name': name, 'args': args, 'kwargs': kwargs}
                    if not self.q:
                        return False
                    queueNum, lock = self.q.myput(tmp, self.result)
                    with lock:
                        self.logger.info("%s query number %s added to queue with args=(%s)" % (name, queueNum, str(tmp)))
                        _lock_time = time()
                        lock.wait()#wait_for(lambda: self.result[queueNum][0])
                        _lock_time = time() - _lock_time
                        self.logger.debug("%s query number lock time = %.4f" % (queueNum, _lock_time))
                        done, result = self.result[queueNum]
                        self.result.pop(queueNum)
                    return result
                except Exception as e:
                    self.logger.exception('query handle function')                    
            
            return handlerFunction
        except Exception as e:
            self.logger.exception('adding query to queue')

#     def getResult(self,queueNum):
#         while self.result[queueNum] == False:
#             continue
#         

    def DBsStatus(self):
        inUse=[]
        notInUse=[]
        for db in self.db:
            if db.inUse:
                inUse.append(self.db.index(db))
            else :
                notInUse.append(self.db.index(db))
        return {'inUse':inUse,'notInUse':notInUse}

    def stop(self):
        while not self.q.empty():
            lock, queueNum, query = self.q.get()
            with lock:
                self.result[queueNum] = True, False
                lock.notify()
        del self.q
        for i in self.db:
            i.stop()
