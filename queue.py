from queue import Queue
from threading import Condition, RLock
from time import time


class q(Queue):
    def myput(self, item, res_obj, *args, **kwargs):
        if 'lock' in kwargs.keys():
            lock = kwargs['lock']
        else:
            lock = Condition()

        if 'id' in kwargs.keys():
            _id = kwargs['id']
        else:
            _id = time()
        
        res_obj[_id] = [False, False]
        item = [lock, _id, item]
        self.put(item)
        return _id, lock
