import logging

config = {
    'db': {
        'user': 'root',
        'passwd': '******',
        'host': '127.0.0.1',
        'port': '3306',
        'db': 'db_name',
        'logLevel': 'warning',
        'connectionLimit': '30',
        'configFile': '/etc/mysql/debian.cnf',
        'db_object': 'db.MySQLDataBase'
    },
    'log': {
        'handler': {
            'fileHandler': {
                'handler': logging.FileHandler,
                'config': {
                    'filename': '/var/log/ddb.log'
                },
            },
            'streamHandler': {
                'handler': logging.StreamHandler,
            # },
            # 'socketHandler': {
            #     'handler': logging.handlers.SocketHandler,
            #     'config': {
            #         'host': 'localhost',
            #         'port': logging.handlers.DEFAULT_TCP_LOGGING_PORT
            #     },
            }
        },
        'formatter': {
            'stringFormat': '[%(asctime)s] %(levelname)s\t%(name)s(%(module)s@%(lineno)d)\t => %(message)s',
            'dateFormat': '%Z -- %Y-%m-%d %H:%M:%S'
        },
        'defaultLevel': 'warning'
    }
}