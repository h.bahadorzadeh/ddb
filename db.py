#!/usr/bin/env python3.2
import mysql.connector as mdb
from libs.mainLogger import dbLogger as dbLog
from time import time
class MySQLDataBase:
    '''
        A Class to connect to mysql database
        
        Methods:
            _config: set connection parameters
            _connect: connect to db using config parameters
            _disconnect: close connction to database
            select: select row(s) depends on column names which can have condition or not.
            select_by_id: select a row by its ID
            insert: insert a row in db
            delete: delete row(s), which can have condition or not.
            delete_by_id: delete a row by its ID
            update: update row(s) by condition
            update_by_id: update row by its ID
            
        Variables:
            _connect_params: a dictionary contains parameters for connecting to DB
            _connection: store connection
            _cursor: store cursor of connection 
            
    '''
   
    def __init__(self, *args, **kwargs):
        self._connection = None
        self.logger = dbLog.logger
        self._connect(*args, **kwargs)
    
    def __del__(self):
        '''
        '''
        self._disconnect()
        self.logger.info('db deleted')

        
    def _connect(self, *args, **kwargs):
        ''' None -> boolean
        
            Connect to DB using by _config
        '''
        try:
            self._connection = mdb.connect(**kwargs)
            self.logger.info('db connected')
            return True
        except Exception as err:
            self.logger.exception('failed to connect to db')
            self._connection = ""
            return False
    
    def _disconnect(self):
        ''' None -> None
        
            Disconnect from database
        '''
        if self._connection:
            self._connection.close()
            self._connection=None
        self.logger.info('db connection closed')
    def _exec (self, func_name):
        _dict = {
                'select'         : self.select,
                'select_by_id'   : self.select_by_id,
                'insert'         : self.insert,
                'delete'         : self.delete,
                'delete_by_id'   : self.delete_by_id,
                'update'         : self.update,
                'update_by_id'   : self.update_by_id,
                'query'          : self.query,
                'callproc'       : self.callproc,
                'interrupt_query': self.interrupt_query
            }
        if _dict[func_name] :
            return _dict[func_name]
        else:
            return lambda *args, **kwargs : (False, False,)

    def select(self, 
               table_names, 
               columns, 
               conditions=None, 
               order_by=None, 
               sort="ASC", 
               limit=None, 
               with_columns_name=False
               ):
        ''' (list, list, string, string, string, int, boolean) -> List of tuples
            
            Returns list of tuples based on select results
        '''
        # Convert list to string
        columns_as_string = str(','.join(columns))
        tables_as_string = str(','.join(table_names))
        
        list_of_results = []
        columns_name = ()
        
        #If connected to DB do:
        #if(self._connect(self._connect_params)):
        try:
            cursor = self._connection.cursor()
            #Execution of select query
            query = "SELECT %(columns)s FROM %(tables)s"%{
               'columns': columns_as_string,
               'tables': tables_as_string
            }
            if conditions :
                query += " WHERE %s" % str(conditions)
            if order_by :
               query += " ORDER BY %s %s"%(order_by, sort)
            if limit :
               query += " LIMIT %s" % str(limit) 
            _exec_time = time()
            cursor.execute(query)
            _exec_time = time() - _exec_time
            columns_name = cursor.column_names
            _fetch_time = time() 
            list_of_results = cursor.fetchall()
            _fetch_time = time() - _fetch_time
            self.logger.debug('select-query (exec time=%.4f, fetch time=%.4f)= "%s" '%(_exec_time, _fetch_time, query))
#            self._disconnect()
            #self._connection.commit()
            _close_time = time()
            cursor.close()
            _close_time = time() - _close_time
            self.logger.debug("cursor close time = %.4f"%_close_time)
        except Exception as err:
            self.logger.exception('')
            pass
        #Return list of result
        if(with_columns_name):
            return list_of_results, columns_name
        else:
            return list_of_results
    
    def select_by_id(self,
                     table_names, 
                     columns, 
                     ID, 
                     ID_column_name='ID', 
                     order_by='ID', 
                     sort='ASC',
                     have_limit=1000, 
                     with_columns_name=False
                     ):
        '''(list, list, string, string, string, int, boolean) -> List of tuples
        
            Returns a list of tuples in result ID_column_name == ID
        '''
        return self.select([table_names], 
                           [columns], 
                           ID_column_name + " = '" + str(ID) + "'", 
                           order_by, 
                           sort, 
                           have_limit, 
                           with_columns_name
                           )
    
    def insert(self, table_name, columns, values):
        '''(string, list, list)->int
        
            Insert values in table_name and returns the LAST_INSERT_ID()
            
            Preconditions:
                if columns defined count of them must be equal to number of values
        '''
        # Precondition check
        if(len(columns) > 0 and len(columns) != len(values)):
            return 0
                 
        # Convert list to string
        columns_as_string = ','.join(columns)
        ##If there are columns put them in ()
        if(len(columns_as_string) != 0):
            columns_as_string = "(" + columns_as_string + ")"

        #add ' to start and end of string and also there isn't any value makes ''
        values_as_string = "','".join(values)
        if (len(values_as_string) > 1):
            values_as_string = "'" + values_as_string + "'"
        elif (len(values_as_string) == 0):
            values_as_string = "''"
            
        #If connected to db:
        #if(self._connect(self._connect_params)):
        try:
            cursor = self._connection.cursor()
            #Execution of select query
            query = "INSERT INTO %(table_name)s %(columns)s VALUES (%(values)s)" \
                % {
                   'table_name': table_name,
                   'columns': columns_as_string,
                   'values': values_as_string,
                   }
            _exec_time = time()
            cursor.execute(query)
            _exec_time = time() - _exec_time
            self.logger.debug('insert-query (exec time=%.4f)= "%s" '%(_exec_time, query))
            last_id = cursor.lastrowid
            
            # Make sure data is committed to the database
#            self._connection.commit()
#            self._disconnect()
            self._connection.commit()
            _close_time = time()
            cursor.close()
            _close_time = time() - _close_time
            self.logger.debug("cursor close time = %.4f"%_close_time)
            return last_id
        except Exception as err:
            self.logger.exception('')
            pass
        
        return 0
    
    def delete(self, table_name, conditions):
        '''(string, string) -> boolean 
            
            Delete from table_name depends on conditions
        '''
        #If connected to db:
        try:
            cursor = self._connection.cursor()
            #Execution of select query
            query = "DELETE FROM %(table)s WHERE %(conditions)s" % {
                                                    'table': table_name,
                                                    'conditions' : conditions,
                                                    }
            _exec_time = time()
            result = cursor.execute(query)
            _exec_time = time() - _exec_time
            self.logger.debug('delete-query (exec time=%.4f)= "%s" '%(_exec_time, query))
            
            # Make sure data is committed to the database
            if(bool(result)):
                self._connection.commit()
            _close_time = time()
            cursor.close()
            _close_time = time() - _close_time
            self.logger.debug("cursor close time = %.4f"%_close_time)
        except Exception as err:
            self.logger.exception('')
            return False
        
        return bool(result)
    
    def delete_by_id(self, table_name, ID, ID_column_name='ID'):
        '''(string, int, string)->boolean
            
            Delete from table_name where value of ID_column_name is equal to ID
        '''
        return self.delete(table_name, ID_column_name + " = " + str(ID))
    
    def update(self, table_name, columns, values, conditions):
        '''(string, list, list, string)->boolean
        
            Update a row in table_name where conditions are true
            
            Precondition: len(columns) == len(values)
        '''
        if(len(columns) != len(values)):
            return False
        
        #making SET string:
        set_string = ""
        for i in range(len(columns)):
            set_string += columns[i] + " = '" + values[i] + "', "
        #remove last ,
        set_string = set_string[:-2]
        
        try:
            cursor = self._connection.cursor()
            #Execution of select query
            query = "UPDATE %(table)s SET %(set)s WHERE %(conditions)s" \
                % {
                   'table': table_name,
                   'set': set_string,
                   'conditions': conditions,
                   } 
            _exec_time = time()
            result = cursor.execute(query)
            _exec_time = time() - _exec_time
            self.logger.debug('update-query (exec time=%.4f)= "%s" '%(_exec_time, query))
            
            # Make sure data is committed to the database
            if(bool(result)):
                self._connection.commit()
            _close_time = time()
            cursor.close()
            _close_time = time() - _close_time
            self.logger.debug("cursor close time = %.4f"%_close_time)
            del cursor
        except Exception as err:
            self.logger.exception('')
            return False

        return bool(result)
    
    def update_by_id(self, table_name, columns, values, ID, ID_column_name='ID'):
        '''(string, list, list, int, string)->boolean
        
            Update a row depends its ID
        '''
        return self.update(table_name, columns, values, ID_column_name + ' = ' + str(ID))
    
    def query(self, query_string):
        ''' (string) -> boolean, tuple of tuple
            
            Run query_string and returns its result
        '''
        self.logger.info('custom-query = "%s"'%(query_string))
        try:
            try:
                cursor = self._connection.cursor()
                _exec_time = time()
                result = cursor.execute(query_string)
                _exec_time = time() - _exec_time
            except mdb.ProgrammingError:
                self.logger.exception("db-query (%s) syntax error"%query_string)
                raise Exception
            except Exception as err:
                self.logger.exception("db-query (%s) fatal error %r" % (query_string,err))
                raise Exception
        except Exception as e:
            
            cursor.close()
            return False, (e,)
        self._connection.commit()
        if(query_string.upper().find("INSERT") > -1 or \
           query_string.upper().find("UPDATE") > -1 or \
           query_string.upper().find("DELETE") > -1):
           self._connection.commit()
           result = cursor.lastrowid
           self.logger.debug('query (exec time=%.4f)= "%s" '%(_exec_time, query_string))
        else:
            _fetch_time = time()
            result = cursor.fetchall()
            _fetch_time = time() - _fetch_time()
            self.logger.debug('query (exec time=%.4f, fetch time=%.4f)= "%s" '%(_exec_time, _fetch_time, query_string))
        self.logger.debug('db-query: %s  => %s' % (query_string,result))
        _close_time = time()
        cursor.close()
        _close_time = time() - _close_time
        self.logger.debug("cursor close time = %.4f"%_close_time)
        if type(result) == int:
            return True, ((result,),)
        else:
            return True, result
        
        return False, ((),)

    def callproc(self, procname, args=()):
        '''
            call routines and procedures
        '''

        try:
            cursor = self._connection.cursor()
            
            _exec_time = time()
            cursor.callproc(procname, args)
            _exec_time = time() - _exec_time
            _fetch_time = time()
            cursor.fetchone()
            _fetch_time = time() - _fetch_time
            self.logger.debug('callproc-query (exec time=%.4f, fetch time = %.4f)= "%s" '%(_exec_time, _fetch_time, procname))

            self._connection.commit()
            _close_time = time()
            cursor.close()
            _close_time = time() - _close_time
            self.logger.debug("cursor close time = %.4f"%_close_time)
            
        except Exception as err:
            self.logger.exception('')
            return False
        
        return True

    def interrupt_query(self):
        '''
            stops query and delete cursor and close connection
        '''

        try:
            self._connection.commit()
            cursor.close()
            return True
        except Exception as e:
            self.logger.exception('interrupting query')
            return False
