import threading
import time


class dbThread(threading.Thread):
    def __init__(self, db_thread_manager, safe_mode):
        self._id = None
        self.dbThreadManager = db_thread_manager
        self.db_object = db_thread_manager.db_object
        self.logger = db_thread_manager.logger
        self._stopEvent = threading.Event()
        threading.Thread.__init__(self, name='db-Connection')
        self.setDaemon(1)
        self.inUse = False
        self.timer = None
        self.safe_mode = safe_mode
        try:
            self.db = self.db_object(self.dbThreadManager.dbLog, **self.dbThreadManager.db_parameters)
            self.logger.info('db thread created')
        except Exception as e:
            self.logger.exception('creating db thread')

    def reconnect(self):
        self.db._connection = False
        try:
            self.db._disconnect()
        except Exception as e:
            pass
        connected = False
        while not connected:
            try:
                time.sleep(1)
                # del self.db
                self.db = self.db_object(self.dbThreadManager.dbLog, **self.dbThreadManager.db_parameters)
                self.logger.debug('reconnected')
                connected = True
            # self.db._connect(**self.dbThreadManager.db_parameters)
            except Exception as e:
                self.db._connection = False
                self.logger.exception('reconnecting failed')
        if not connected:
            self.reconnect()

    def run(self):
        while not self._stopEvent.is_set():
            query = self.dbThreadManager.q.get()
            if not self._stopEvent.is_set():
                self.inUse = True
                self.process_query(query)
                self.inUse = False

    def process_query(self, query_obj):
        try:
            lock, queueNum, query = query_obj
            if not self.db._connection:
                raise Exception('OperationalError')

            # lock, queueNum, query = query_obj
            # if not self.safe_mode == False:
            # self.timer=threading.Timer(self.safe_mode, self.check, _id=queueNum, query=query, lock=lock)
            #     self.timer.start()

            with lock:
                self.logger.info(
                    "%s => self.db.%s(*%s,**%s)" % (self._id, query['name'], query['args'], query['kwargs']))
                tmp = eval("self.db.%s(*%s,**%s)" % (query['name'], query['args'], query['kwargs']))
                self.logger.debug("%s => after eval %r" % (self._id, tmp))
                if type(tmp) == tuple:
                    if tmp[0] == False:
                        raise Exception(tmp[1][0])
                    else:
                        self.dbThreadManager.result[queueNum] = tmp
                else:
                    self.dbThreadManager.result[queueNum] = True, tmp
                self.logger.debug(
                    "%s => query proccess result %r" % (self._id, self.dbThreadManager.result[queueNum][1]))
                lock.notify()
                if self.timer:
                    self.timer.cancel()
                return True

        except Exception as e:
            self.reconnect()
            self.dbThreadManager.q.myput(query, self.dbThreadManager.result, id=queueNum, lock=lock)
            self.logger.exception('db thread proccess query')
            return False

        if type(tmp) == tuple:
            self.dbThreadManager.result[queueNum] = tmp
        else:
            self.dbThreadManager.result[queueNum] = True, tmp
        self.logger.debug("query proccess result %r" % self.dbThreadManager.result[queueNum][1])
        lock.notify()
        return True

    def stop(self):
        self._stopEvent.set()
        try:
            if self.db._connection != None:
                self.db._disconnect()
        except Exception as e:
            self.logger.exceptin('stopping db thread')
            #
            # def check(self, _id, query, lock):
            # if _id in self.dbThreadManager.result.keys():
            #         if not self.dbThreadManager.result[_id][0]:
            #             self.logger.warning('Failed to get query "%r"' %(query))
            #             if self.db.interrupt_query():
            #                 self.dbThreadManager.q.put(query, id=_id, lock=lock)
            #                 self.dbThreadManager.getConnection()
            #             else:
            #                 self.logger.error('Failed to interrupt query "%r"!!!!' %(query))
            #         else:
            #             self.logger.warning('query %s is done but result is not taken, try to notify again' %(query))
            #             with lock:
            #                 lock.notify()
            #     else:
            #         self.logger.debug('query %s check is ok :)' % _id)
