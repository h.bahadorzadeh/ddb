import logging, queue
from logging.handlers import QueueHandler, QueueListener

class log:
    def __init__(self,config, level=False,name=''):
        if level == False:
            level = config.get('log').get('defaultLevel')
        formatter_config=eval(config.get('log').get('formatter'))
        self.handlerConfig=eval(config.get('log').get('handler'))
        self.handlers=[]
        self.level=level.upper()
        self.loggersList={}
        self.que = queue.Queue(-1) # no limit on size
        self.queue_handler = QueueHandler(self.que)
        formatter = logging.Formatter(formatter_config['stringFormat'],
                                      formatter_config['dateFormat'])
        if len (self.handlerConfig)>0:
            for i in self.handlerConfig:
                conf=self.handlerConfig[i]['config'] if self.handlerConfig[i].get('config') else {}
                self.handlers.append(self.handlerConfig[i]['handler'](**conf))
                self.handlers[len(self.handlers)-1].setFormatter(formatter)
        else :
            self.handlers.append(logging.NullHandler())
            
        self.listener = QueueListener(self.que,*self.handlers)
        if name=='':
            self.logger=logging.getLogger('logger')
        else :
            self.logger=logging.getLogger(str(name))
        self.logger.setLevel(self.level)
        self.logger.addHandler(self.queue_handler)
        self.listener.start()
        self.logger.info('logger starterd with level "%s"'%(self.level))
        
    def addLogger(self, name):
        self.logger.info('logger "%s" added with level "%s"'%(name, self.level))
        logger=logging.getLogger(name)
        logger.setLevel(self.level)
        logger.addHandler(self.queue_handler)
        self.loggersList[name]=logger
        return self.loggersList[name]
        
    def stop(self):
        self.listener.stop()
        tmp=[]
        for logger in self.loggersList:
            tmp.append(logger)
        for logger in tmp:
            del self.loggersList[logger]
        del self.logger